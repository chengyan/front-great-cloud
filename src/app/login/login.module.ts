import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {LoginComponent} from "./login.component";
import {NgZorroAntdModule} from "ng-zorro-antd";
import {ReactiveFormsModule} from "@angular/forms";


@NgModule({
  declarations: [
    LoginComponent
  ],
  imports: [
    BrowserModule,
    NgZorroAntdModule.forRoot(),
    ReactiveFormsModule
  ],
  exports: [
    LoginComponent
  ],
  providers: [],
})
export class LoginModule { }
